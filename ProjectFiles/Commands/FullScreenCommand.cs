﻿namespace Codefarts.Commander.UnityCommands
{
    using System.Collections.Generic;

    using UnityEngine;

    public class FullScreenCommand
    {
        public static IDictionary<string, object> FullScreen(IDictionary<string, object> arguments)
        {
            var result = new Dictionary<string, object>();
            if (arguments != null && arguments.ContainsKey("state"))
            {
                bool fullScreen;
                if (bool.TryParse(arguments["state"].ToString(), out fullScreen))
                {
                    Screen.fullScreen = fullScreen;
                }
            }

            result["result"] = Screen.fullScreen;
            return result;
        }
    }
}
