﻿namespace Codefarts.Commander.Scripts
{
    using System.Collections.Generic;

    using Codefarts.Commander.UnityCommands;

    using UnityEngine;

    public class ConsoleWindow : MonoBehaviour
    {
        private CommandManager<string, IDictionary<string, object>> manager;

        private Rect screenRect = new Rect(0, 0, 300, 250);

        private string commandLine = string.Empty;

        private string history = string.Empty;

        private Vector2 scroll;

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.manager = new CommandManager<string, IDictionary<string, object>>();
            this.manager.Register<IDictionary<string, object>>("fullscreen", FullScreenCommand.FullScreen);
        }

        /// <summary>
        /// OnGUI is called for rendering and handling GUI events.
        /// </summary>
        public void OnGUI()
        {
            this.screenRect = GUILayout.Window(1, this.screenRect, this.DrawWindow, "Console");
        }

        private void DrawWindow(int id)
        {
            GUILayout.BeginVertical();

            // history
            this.scroll = GUILayout.BeginScrollView(this.scroll, false, false, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
            this.history = GUILayout.TextArea(this.history);
            GUILayout.EndScrollView();


            GUILayout.BeginHorizontal(); // begin commandline

            this.commandLine = GUILayout.TextField(this.commandLine, GUILayout.ExpandWidth(true));

            if (GUILayout.Button("Execute", GUILayout.ExpandWidth(false)))
            {
                this.ExecuteCommand();
            }

            GUILayout.EndHorizontal();   // end commandline

            GUILayout.EndVertical();

            GUI.DragWindow();
        }

        private void ExecuteCommand()
        {
            var query = string.Empty;
            var index = this.commandLine.IndexOf("?");
            if (index != -1)
            {
                query = this.commandLine.Substring(index).Trim();
                this.commandLine = this.commandLine.Substring(0, index);
            }

            var arguments = this.ParseQueryString(query);
            var response = this.manager.Execute(this.commandLine, arguments);

            if (response != null && response.ContainsKey("result"))
            {
                this.history += "\r\n" + response["result"];
            }

            this.commandLine = string.Empty;
        }

        public IDictionary<string, object> ParseQueryString(string queryString)
        {
            var queryParameters = new Dictionary<string, object>();
            if (string.IsNullOrEmpty(queryString))
            {
                return queryParameters;
            }

            var querySegments = queryString.Split('&');

            foreach (var segment in querySegments)
            {
                var parts = segment.Split('=');
                if (parts.Length > 0)
                {
                    var key = parts[0].Trim(new char[] { '?', ' ' });
                    var val = parts[1].Trim();

                    queryParameters.Add(key, val);
                }
            }

            return queryParameters;
        }
    }
}